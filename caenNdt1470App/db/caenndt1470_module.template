###############################################################################
#
# EPICS database template for CAEN NDT-1470 NIM and Desktop HV Power Supply
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, Lund, 2019
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
#
# -----------------------------------------------------------------------------
# Based on 'CAENNDT.tpl' provided by the manufacturer; but with a bunch of
# modifications by ESS.
#
###############################################################################

record(stringin, "$(P)$(R)Name-R") {
    field(VAL,  "CAEN-NDT1470")
    field(DESC, "Module name")
    field(DTYP, "stream")
    field(PINI, "YES")
    field(INP,  "@caenNdt1470.proto getBDNAME($(BD)) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)NumCh-R") {
    field(DESC, "Nr of channels present")
    field(DTYP, "stream")
    field(PINI, "YES")
    field(INP,  "@caenNdt1470.proto getBDNCH($(BD)) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)FwRel-R") {
    field(DESC, "Module Firmware Release")
    field(DTYP, "stream")
    field(PINI, "YES")
    field(INP,  "@caenNdt1470.proto getBDFREL($(BD)) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)SerialNum-R") {
    field(DESC, "Module Serial Number")
    field(DTYP, "stream")
    field(PINI, "YES")
    field(INP,  "@caenNdt1470.proto getBDSNUM($(BD)) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
}

record(bi, "$(P)$(R)IlkStatus-R") {
    field(DESC, "Module Interlock status")
    field(DTYP, "stream")
    field(ZNAM, "NO")
    field(ONAM, "YES")
    field(PINI, "YES")
    field(SCAN, "$(SCAN) second")
    field(INP,  "@caenNdt1470.proto getBDILK($(BD)) $(PORT) $(ADDR)")
    field(FLNK, "$(P)$(R)IlkMode-R")
}

record(bo, "$(P)$(R)IlkMode-S") {
    field(DESC, "Module Interlock mode")
    field(DTYP, "Soft Channel")
    field(ZNAM, "CLOSED")
    field(ONAM, "OPEN")
    field(OUT,  "$(P)$(R)#IlkMode-S PP")
}

record(bo, "$(P)$(R)#IlkMode-S") {
    field(SDIS, "$(P)$(R)IlkModeSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(ZNAM, "CLOSED")
    field(ONAM, "OPEN")
    field(OUT,  "@caenNdt1470.proto setBDILKM($(BD), $(P)$(R)IlkModeMsg) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)IlkModeMsg") {
    field(DESC, "Interlock mode set status")
    field(DTYP, "Soft Channel")
}

record(bi, "$(P)$(R)IlkMode-R") {
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getBDILKM($(BD)) $(PORT) $(ADDR)")
    field(ZNAM, "CLOSED")
    field(ONAM, "OPEN")
    field(FLNK, "$(P)$(R)IlkModeSync")
}

record(bo, "$(P)$(R)IlkModeSync") {
    field(DOL,  "$(P)$(R)IlkMode-R NPP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)IlkMode-S PP")
    field(FLNK, "$(P)$(R)CtrlMode-R")
}

record(bi, "$(P)$(R)CtrlMode-R") {
    field(DESC, "Module Control mode")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getBDCTR($(BD)) $(PORT) $(ADDR)")
    field(ZNAM, "REMOTE")
    field(ONAM, "LOCAL")
    field(PINI, "YES")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)BusTerm-R")
}

record(bi, "$(P)$(R)BusTerm-R") {
    field(DESC, "Local Bus Termination status")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getBDTERM($(BD)) $(PORT) $(ADDR)")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
    field(PINI, "YES")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)Alarm")
}

record(mbbiDirect, "$(P)$(R)Alarm") {
    field(DESC, "Module Alarm status")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getBDALARM($(BD)) $(PORT) $(ADDR)")
    field(PINI, "YES")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)#CalcModuleStatus")
}

record(fanout, "$(P)$(R)#CalcModuleStatus") {
    field(LNK1, "$(P)$(R)AlarmCh0-R")
    field(LNK2, "$(P)$(R)AlarmCh1-R")
    field(LNK3, "$(P)$(R)AlarmCh2-R")
    field(LNK4, "$(P)$(R)AlarmCh3-R")
    field(LNK5, "$(P)$(R)AlarmPwFail-R")
    field(LNK6, "$(P)$(R)AlarmOvP-R")
    field(FLNK, "$(P)$(R)AlarmHvckFail-R")
}

record(bi, "$(P)$(R)AlarmCh0-R") {
    field(DESC, "Ch0 in Alarm status")
    field(INP,  "$(P)$(R)Alarm.B0 MS")
    field(ZNAM, "Ch0 is OK")
    field(ONAM, "Ch0 in Alarm status")
    field(OSV,  "MAJOR")
}

record(bi, "$(P)$(R)AlarmCh1-R") {
    field(DESC, "Ch1 in Alarm status")
    field(INP,  "$(P)$(R)Alarm.B1 MS")
    field(ZNAM, "Ch1 is OK")
    field(ONAM, "Ch1 in Alarm status")
    field(OSV,  "MAJOR")
}

record(bi, "$(P)$(R)AlarmCh2-R") {
    field(DESC, "Ch2 in Alarm status")
    field(INP,  "$(P)$(R)Alarm.B2 MS")
    field(ZNAM, "Ch2 is OK")
    field(ONAM, "Ch2 in Alarm status")
    field(OSV,  "MAJOR")
}

record(bi, "$(P)$(R)AlarmCh3-R") {
    field(DESC, "Ch3 in Alarm status")
    field(INP,  "$(P)$(R)Alarm.B3 MS")
    field(ZNAM, "Ch3 is OK")
    field(ONAM, "Ch3 in Alarm status")
    field(OSV,  "MAJOR")
}

record(bi, "$(P)$(R)AlarmPwFail-R") {
    field(DESC, "Board in POWER FAIL")
    field(INP,  "$(P)$(R)Alarm.B4 MS")
    field(ZNAM, "Power is OK")
    field(ONAM, "Board in POWER FAIL")
    field(OSV,  "MAJOR")
}

record(bi, "$(P)$(R)AlarmOvP-R") {
    field(DESC, "Board in OVER POWER")
    field(INP,  "$(P)$(R)Alarm.B5 MS")
    field(ZNAM, "Power is OK")
    field(ONAM, "Board in OVER POWER")
    field(OSV,  "MAJOR")
}

record(bi, "$(P)$(R)AlarmHvckFail-R") {
    field(DESC, "Internal HV Clock FAIL")
    field(INP,  "$(P)$(R)Alarm.B6 MS")
    field(ZNAM, "HV Clock is OK")
    field(ONAM, "Internal HV Clock FAIL")
    field(OSV,  "MAJOR")
}

record(bo, "$(P)$(R)ClrAlarmSign-S") {
    field(DESC, "Clear Alarm signal")
    field(DTYP, "stream")
    field(ONAM, "Process")
    field(OUT,  "@caenNdt1470.proto setBDCLR($(BD), $(P)$(R)ClrAlarmMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)ClrAlarmMsg-R") {
    field(DESC, "Clear alarms status")
    field(DTYP, "Soft Channel")
}

