###############################################################################
#
# EPICS database template for CAEN NDT-1470 NIM and Desktop HV Power Supply
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, Lund, 2019
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
#
###############################################################################

# -----------------------------------------------------------------------------
# Voltage
# -----------------------------------------------------------------------------
record(ai, "$(P)$(R)Ch$(CH)-Voltage-RB") {
    field(DESC, "Read out VSET value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllVSET($(BD), $(P)$(R)Ch0-Voltage-RB, $(P)$(R)Ch1-Voltage-RB, $(P)$(R)Ch2-Voltage-RB, $(P)$(R)Ch3-Voltage-RB) $(PORT) $(ADDR)")
    field(EGU,  "V")
    field(SCAN, "$(SCAN) second")
    field(FLNK, "$(P)$(R)#Ch$(CH)-VoltSync")
}

record(ao, "$(P)$(R)#Ch$(CH)-VoltSync") {
    field(DOL,  "$(P)$(R)Ch$(CH)-Voltage-RB NPP")
    field(OMSL, "closed_loop")
    field(FLNK, "$(P)$(R)Ch$(CH)-Voltage-R")
}

record(ao, "$(P)$(R)Ch$(CH)-Voltage-S") {
    field(DESC, "Voltage Set value")
    field(DTYP, "Soft Channel")
    field(EGU,  "V")
    field(OUT,  "$(P)$(R)#Ch$(CH)-VoltWrite PP")
    field(VAL,  "0")
}

record(ao, "$(P)$(R)#Ch$(CH)-VoltWrite") {
    field(SDIS, "$(P)$(R)#Ch$(CH)-VoltSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@caenNdt1470.proto setVSET($(BD), $(CH), $(P)$(R)Ch$(CH)-VoltMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-VoltMsg-R") {
    field(DESC, "Voltage Set value status")
    field(DTYP, "Soft Channel")
}

record(ai, "$(P)$(R)Ch$(CH)-VoltMinSet-R") {
    field(DESC, "Minimum voltage setted")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllVMIN($(BD), $(P)$(R)Ch0-VoltMinSet-R, $(P)$(R)Ch1-VoltMinSet-R, $(P)$(R)Ch2-VoltMinSet-R, $(P)$(R)Ch3-VoltMinSet-R) $(PORT) $(ADDR)")
    field(EGU,  "V")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-VoltMaxSet-R") {
    field(DESC, "Maximum voltage setted")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllVMAX($(BD), $(P)$(R)Ch0-VoltMaxSet-R, $(P)$(R)Ch1-VoltMaxSet-R, $(P)$(R)Ch2-VoltMaxSet-R, $(P)$(R)Ch3-VoltMaxSet-R) $(PORT) $(ADDR)")
    field(EGU,  "V")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-VoltDecSet-R") {
    field(DESC, "VSet nr of decimal digits")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllVDEC($(BD), $(P)$(R)Ch0-VoltDecSet-R, $(P)$(R)Ch1-VoltDecSet-R, $(P)$(R)Ch2-VoltDecSet-R, $(P)$(R)Ch3-VoltDecSet-R) $(PORT) $(ADDR)")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-Voltage-R") {
    field(DESC, "Voltage Monitor value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllVMON($(BD), $(P)$(R)Ch0-Voltage-R, $(P)$(R)Ch1-Voltage-R, $(P)$(R)Ch2-Voltage-R, $(P)$(R)Ch3-Voltage-R) $(PORT) $(ADDR)")
    field(EGU,  "V")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)Ch$(CH)-VoltMaxLimit-R")
}

record(ai, "$(P)$(R)Ch$(CH)-VoltMaxLimit-R") {
    field(DESC, "Max Voltage Limit")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllMAXV($(BD), $(P)$(R)Ch0-VoltMaxLimit-R, $(P)$(R)Ch1-VoltMaxLimit-R, $(P)$(R)Ch2-VoltMaxLimit-R, $(P)$(R)Ch3-VoltMaxLimit-R) $(PORT) $(ADDR)")
    field(EGU,  "V")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)#Ch$(CH)-VoltMaxSync")
}

record(ao, "$(P)$(R)Ch$(CH)-VoltMaxLimit-S") {
    field(DESC, "MaxVSet value")
    field(DTYP, "Soft Channel")
    field(EGU,  "V")
    field(OUT,  "$(P)$(R)#Ch$(CH)-VoltMaxWrite PP")
}

record(ao, "$(P)$(R)#Ch$(CH)-VoltMaxWrite") {
    field(SDIS, "$(P)$(R)#Ch$(CH)-VoltMaxSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@caenNdt1470.proto setMAXV($(BD), $(CH), $(P)$(R)Ch$(CH)-MaxVoltMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-MaxVoltMsg-R") {
    field(DESC, "MaxVSet value status")
    field(DTYP, "Soft Channel")
}

record(ao, "$(P)$(R)#Ch$(CH)-VoltMaxSync") {
    field(DOL,  "$(P)$(R)Ch$(CH)-VoltMaxLimit-R NPP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)Ch$(CH)-VoltMaxLimit-S PP")
    field(FLNK, "$(P)$(R)Ch$(CH)-Current-RB")
}

record(ai, "$(P)$(R)Ch$(CH)-MaxHwVolt-R") {
    field(DESC, "MaxVSet maximum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllMVMAX($(BD), $(P)$(R)Ch0-MaxHwVolt-R, $(P)$(R)Ch1-MaxHwVolt-R, $(P)$(R)Ch2-MaxHwVolt-R, $(P)$(R)Ch3-MaxHwVolt-R) $(PORT) $(ADDR)")
    field(EGU,  "V")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-MinHwVolt-R") {
    field(DESC, "MaxVSet minimum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllMVMIN($(BD), $(P)$(R)Ch0-MinHwVolt-R, $(P)$(R)Ch1-MinHwVolt-R, $(P)$(R)Ch2-MinHwVolt-R, $(P)$(R)Ch3-MinHwVolt-R) $(PORT) $(ADDR)")
    field(EGU,  "V")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-DecHwVolt-R") {
    field(DESC, "MaxVSet decimal digits")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllMVDEC($(BD), $(P)$(R)Ch0-DecHwVolt-R, $(P)$(R)Ch1-DecHwVolt-R, $(P)$(R)Ch2-DecHwVolt-R, $(P)$(R)Ch3-DecHwVolt-R) $(PORT) $(ADDR)")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

# -----------------------------------------------------------------------------
# Current
# -----------------------------------------------------------------------------
record(ai, "$(P)$(R)Ch$(CH)-Current-RB") {
    field(DESC, "Read out ISET value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllISET($(BD), $(P)$(R)Ch0-Current-RB, $(P)$(R)Ch1-Current-RB, $(P)$(R)Ch2-Current-RB, $(P)$(R)Ch3-Current-RB) $(PORT) $(ADDR)")
    field(EGU,  "uA")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)#Ch$(CH)-CurrSync")
}

record(ao, "$(P)$(R)Ch$(CH)-Current-S") {
    field(DESC, "Current Set value")
    field(DTYP, "Soft Channel")
    field(EGU,  "uA")
    field(OUT,  "$(P)$(R)#Ch$(CH)-CurrWrite PP")
    field(VAL,  "0")
}

record(ao, "$(P)$(R)#Ch$(CH)-CurrWrite") {
    field(SDIS, "$(P)$(R)#Ch$(CH)-CurrSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@caenNdt1470.proto setISET($(BD), $(CH), $(P)$(R)Ch$(CH)-CurrentMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-CurrentMsg-R") {
    field(DESC, "Current Set value status")
    field(DTYP, "Soft Channel")
}

record(ao, "$(P)$(R)#Ch$(CH)-CurrSync") {
    field(DOL,  "$(P)$(R)Ch$(CH)-Current-RB NPP")
    field(OMSL, "closed_loop")
    field(FLNK, "$(P)$(R)Ch$(CH)-Current-R")
}

record(ai, "$(P)$(R)Ch$(CH)-CurrMinSet-R") {
    field(DESC, "ISet minimum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllIMIN($(BD), $(P)$(R)Ch0-CurrMinSet-R, $(P)$(R)Ch1-CurrMinSet-R, $(P)$(R)Ch2-CurrMinSet-R, $(P)$(R)Ch3-CurrMinSet-R) $(PORT) $(ADDR)")
    field(EGU,  "uA")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-CurrMaxSet-R") {
    field(DESC, "ISet maximum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllIMAX($(BD), $(P)$(R)Ch0-CurrMaxSet-R, $(P)$(R)Ch1-CurrMaxSet-R, $(P)$(R)Ch2-CurrMaxSet-R, $(P)$(R)Ch3-CurrMaxSet-R) $(PORT) $(ADDR)")
    field(EGU,  "uA")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-CurrDecSet-R") {
    field(DESC, "ISet nr of decimal digits")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllISDEC($(BD), $(P)$(R)Ch0-CurrDecSet-R, $(P)$(R)Ch1-CurrDecSet-R, $(P)$(R)Ch2-CurrDecSet-R, $(P)$(R)Ch3-CurrDecSet-R) $(PORT) $(ADDR)")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-Current-R") {
    field(DESC, "Current Monitor value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllIMON($(BD), $(P)$(R)Ch0-Current-R, $(P)$(R)Ch1-Current-R, $(P)$(R)Ch2-Current-R, $(P)$(R)Ch3-Current-R) $(PORT) $(ADDR)")
    field(EGU,  "uA")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)Ch$(CH)-CurrModeRange-R")
}

record(bi, "$(P)$(R)Ch$(CH)-CurrModeRange-R") {
    field(DESC, "Read out IMON RANGE value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllIMRANGE($(BD), $(P)$(R)Ch0-CurrModeRange-R, $(P)$(R)Ch1-CurrModeRange-R, $(P)$(R)Ch2-CurrModeRange-R, $(P)$(R)Ch3-CurrModeRange-R) $(PORT) $(ADDR)")
    field(ZNAM, "HIGH")
    field(ONAM, "LOW")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)#Ch$(CH)-CurrModeSync")
}

record(bo, "$(P)$(R)Ch$(CH)-CurrModeRange-S") {
    field(DESC, "IMon Range value")
    field(DTYP, "Soft Channel")
    field(ZNAM, "HIGH")
    field(ONAM, "LOW")
    field(OUT,  "$(P)$(R)#Ch$(CH)-CurrModeWrite PP")
}

record(bo, "$(P)$(R)#Ch$(CH)-CurrModeWrite") {
    field(SDIS, "$(P)$(R)#Ch$(CH)-CurrModeSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(ZNAM, "HIGH")
    field(ONAM, "LOW")
    field(OUT,  "@caenNdt1470.proto setIMRANGE($(BD), $(CH), $(P)$(R)Ch$(CH)-CurrModeMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-CurrModeMsg-R") {
    field(DESC, "IMonRange Set value status")
    field(DTYP, "Soft Channel")
}

record(bo, "$(P)$(R)#Ch$(CH)-CurrModeSync") {
    field(DOL,  "$(P)$(R)#Ch$(CH)-CurrModeRange-R NPP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)Ch$(CH)-CurrModeRange-S PP")
    field(FLNK, "$(P)$(R)Ch$(CH)-CurrentDec-R")
}

record(ai, "$(P)$(R)Ch$(CH)-CurrentDec-R") {
    field(DESC, "IMon nr of decimal digits")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllIMDEC($(BD), $(P)$(R)Ch0-CurrentDec-R, $(P)$(R)Ch1-CurrentDec-R, $(P)$(R)Ch2-CurrentDec-R, $(P)$(R)Ch3-CurrentDec-R) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)Ch$(CH)-RampUp-R")
}


# -----------------------------------------------------------------------------
# Ramp Up / Down
# -----------------------------------------------------------------------------
record(ai, "$(P)$(R)Ch$(CH)-RampUp-R") {
    field(DESC, "Read out RAMP UP value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllRUP($(BD), $(P)$(R)Ch0-RampUp-R, $(P)$(R)Ch1-RampUp-R, $(P)$(R)Ch2-RampUp-R, $(P)$(R)Ch3-RampUp-R) $(PORT) $(ADDR)")
    field(EGU,  "V/s")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)#Ch$(CH)-RampUpSync")
}

record(ao, "$(P)$(R)Ch$(CH)-RampUp-S") {
    field(DESC, "Ramp Up value")
    field(DTYP, "Soft Channel")
    field(EGU,  "V/s")
    field(OUT,  "$(P)$(R)#Ch$(CH)-RampUpWrite PP")
    field(VAL,  "0")
}

record(ao, "$(P)$(R)#Ch$(CH)-RampUpWrite") {
    field(SDIS, "$(P)$(R)#Ch$(CH)-RampUpSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@caenNdt1470.proto setRUP($(BD), $(CH), $(P)$(R)Ch$(CH)-RampUpMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-RampUpMsg-R") {
    field(DESC, "RampUp Set value status")
    field(DTYP, "Soft Channel")
}

record(ao, "$(P)$(R)#Ch$(CH)-RampUpSync") {
    field(DOL,  "$(P)$(R)Ch$(CH)-RampUp-R NPP")
    field(OMSL, "closed_loop")
    field(FLNK, "$(P)$(R)Ch$(CH)-RampDw-R")
}

record(ai, "$(P)$(R)Ch$(CH)-RampUpMin-R") {
    field(DESC, "RUp minimum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllRUPMIN($(BD), $(P)$(R)Ch0-RampUpMin-R, $(P)$(R)Ch1-RampUpMin-R, $(P)$(R)Ch2-RampUpMin-R, $(P)$(R)Ch3-RampUpMin-R) $(PORT) $(ADDR)")
    field(EGU,  "V/s")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-RampUpMax-R") {
    field(DESC, "RUp maximum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllRUPMAX($(BD), $(P)$(R)Ch0-RampUpMax-R, $(P)$(R)Ch1-RampUpMax-R, $(P)$(R)Ch2-RampUpMax-R, $(P)$(R)Ch3-RampUpMax-R) $(PORT) $(ADDR)")
    field(EGU,  "V/s")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-RampUpDec-R") {
    field(DESC, "RUp nr of decimal digits")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllRUPDEC($(BD), $(P)$(R)Ch0-RampUpDec-R, $(P)$(R)Ch1-RampUpDec-R, $(P)$(R)Ch2-RampUpDec-R, $(P)$(R)Ch3-RampUpDec-R) $(PORT) $(ADDR)")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-RampDw-R") {
    field(DESC, "Read out RAMP DOWN value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllRDW($(BD), $(P)$(R)Ch0-RampDw-R, $(P)$(R)Ch1-RampDw-R, $(P)$(R)Ch2-RampDw-R, $(P)$(R)Ch3-RampDw-R) $(PORT) $(ADDR)")
    field(EGU,  "V/s")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)#Ch$(CH)-RampDwSync")
}

record(ao, "$(P)$(R)Ch$(CH)-RampDw-S") {
    field(DESC, "Ramp Down value")
    field(DTYP, "Soft Channel")
    field(EGU,  "V/s")
    field(OUT,  "$(P)$(R)#Ch$(CH)-RampDwWrite PP")
    field(VAL,  "0")
}

record(ao, "$(P)$(R)#Ch$(CH)-RampDwWrite") {
    field(SDIS, "$(P)$(R)#Ch$(CH)-RampDwSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@caenNdt1470.proto setRDW($(BD), $(CH), $(P)$(R)Ch$(CH)-RampDwMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-RampDwMsg-R") {
    field(DESC, "RampDown Set value status")
    field(DTYP, "Soft Channel")
}

record(ao, "$(P)$(R)#Ch$(CH)-RampDwSync") {
    field(DOL,  "$(P)$(R)Ch$(CH)-RampDw-R NPP")
    field(OMSL, "closed_loop")
    field(FLNK, "$(P)$(R)Ch$(CH)-Trip-R")
}

record(ai, "$(P)$(R)Ch$(CH)-RampDwMin-R") {
    field(DESC, "RDw minimum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllRDWMIN($(BD), $(P)$(R)Ch0-RampDwMin-R, $(P)$(R)Ch1-RampDwMin-R, $(P)$(R)Ch2-RampDwMin-R, $(P)$(R)Ch3-RampDwMin-R) $(PORT) $(ADDR)")
    field(EGU,  "V/s")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-RampDwMax-R") {
    field(DESC, "RDw maximum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllRDWMAX($(BD), $(P)$(R)Ch0-RampDwMax-R, $(P)$(R)Ch1-RampDwMax-R, $(P)$(R)Ch2-RampDwMax-R, $(P)$(R)Ch3-RampDwMax-R) $(PORT) $(ADDR)")
    field(EGU,  "V/s")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-RampDwDec-R") {
    field(DESC, "RDw nr of decimal digits")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllRDWDEC($(BD), $(P)$(R)Ch0-RampDwDec-R, $(P)$(R)Ch1-RampDwDec-R, $(P)$(R)Ch2-RampDwDec-R, $(P)$(R)Ch3-RampDwDec-R) $(PORT) $(ADDR)")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

# -----------------------------------------------------------------------------
# Trip
# -----------------------------------------------------------------------------
record(ai, "$(P)$(R)Ch$(CH)-Trip-R") {
    field(DESC, "Read out TRIP time value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllTRIP($(BD), $(P)$(R)Ch0-Trip-R, $(P)$(R)Ch1-Trip-R, $(P)$(R)Ch2-Trip-R, $(P)$(R)Ch3-Trip-R) $(PORT) $(ADDR)")
    field(EGU,  "s")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)#Ch$(CH)-TripSync")
}

record(ao, "$(P)$(R)Ch$(CH)-Trip-S") {
    field(DESC, "Trip time value")
    field(DTYP, "Soft Channel")
    field(EGU,  "s")
    field(OUT,  "$(P)$(R)#Ch$(CH)-TripWrite PP")
}

record(ao, "$(P)$(R)#Ch$(CH)-TripWrite") {
    field(SDIS, "$(P)$(R)#Ch$(CH)-TripSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@caenNdt1470.proto setTRIP($(BD), $(CH), $(P)$(R)Ch$(CH)-TripMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-TripMsg-R") {
    field(DESC, "TripTime Set value status")
    field(DTYP, "Soft Channel")
}

record(ao, "$(P)$(R)#Ch$(CH)-TripSync") {
    field(DOL,  "$(P)$(R)Ch$(CH)-Trip-R NPP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)Ch$(CH)-Trip-S PP")
    field(FLNK, "$(P)$(R)Ch$(CH)-PowerDwn-R")
}

record(ai, "$(P)$(R)Ch$(CH)-TripMin-R") {
    field(DESC, "Trip time minimum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllTRIPMIN($(BD), $(P)$(R)Ch0-TripMin-R, $(P)$(R)Ch1-TripMin-R, $(P)$(R)Ch2-TripMin-R, $(P)$(R)Ch3-TripMin-R) $(PORT) $(ADDR)")
    field(EGU,  "s")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-TripMax-R") {
    field(DESC, "Trip time maximum value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllTRIPMAX($(BD), $(P)$(R)Ch0-TripMax-R, $(P)$(R)Ch1-TripMax-R, $(P)$(R)Ch2-TripMax-R, $(P)$(R)Ch3-TripMax-R) $(PORT) $(ADDR)")
    field(EGU,  "s")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(ai, "$(P)$(R)Ch$(CH)-TripDec-R") {
    field(DESC, "Trip time nr of decimal digits")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllTRIPDEC($(BD), $(P)$(R)Ch0-TripDec-R, $(P)$(R)Ch1-TripDec-R, $(P)$(R)Ch2-TripDec-R, $(P)$(R)Ch3-TripDec-R) $(PORT) $(ADDR)")
    field(PINI, "YES")
    field(SCAN, "Passive")
}


# -----------------------------------------------------------------------------
# Channel settings
# -----------------------------------------------------------------------------
record(bi, "$(P)$(R)Ch$(CH)-PowerDwn-R") {
    field(DESC, "Read out POWER DOWN value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllPDWN($(BD), $(P)$(R)Ch0-PowerDwn-R, $(P)$(R)Ch1-PowerDwn-R, $(P)$(R)Ch2-PowerDwn-R, $(P)$(R)Ch3-PowerDwn-R) $(PORT) $(ADDR)")
    field(ZNAM, "RAMP")
    field(ONAM, "KILL")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)#Ch$(CH)-PowerDwnSync")
}

record(bo, "$(P)$(R)Ch$(CH)-PowerDwn-S") {
    field(DESC, "Power Down value")
    field(DTYP, "Soft Channel")
    field(ZNAM, "RAMP")
    field(ONAM, "KILL")
    field(OUT,  "$(P)$(R)#Ch$(CH)-PowerDwnWrite PP")
}

record(bo, "$(P)$(R)#Ch$(CH)-PowerDwnWrite") {
    field(SDIS, "$(P)$(R)#Ch$(CH)-PowerDwnSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(ZNAM, "RAMP")
    field(ONAM, "KILL")
    field(OUT,  "@caenNdt1470.proto setPDWN($(BD), $(CH), $(P)$(R)Ch$(CH)-PowerDwnMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-PowerDwnMsg-R") {
    field(DESC, "TripTime Set value status")
    field(DTYP, "Soft Channel")
}

record(bo, "$(P)$(R)#Ch$(CH)-PowerDwnSync") {
    field(DOL,  "$(P)$(R)Ch$(CH)-PowerDwn-R NPP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)Ch$(CH)-PowerDwn-S PP")
    field(FLNK, "$(P)$(R)Ch$(CH)-Stat-R")
}

record(bi, "$(P)$(R)Ch$(CH)-Polarity-R") {
    field(DESC, "Polarity value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllPOL($(BD), $(P)$(R)Ch0-Polarity-R, $(P)$(R)Ch1-Polarity-R, $(P)$(R)Ch2-Polarity-R, $(P)$(R)Ch3-Polarity-R) $(PORT) $(ADDR)")
    field(ZNAM, "+")
    field(ONAM, "-")
    field(PINI, "YES")
    field(SCAN, "Passive")
}

record(mbbiDirect, "$(P)$(R)Ch$(CH)-Stat-R") {
    field(DESC, "Channel Status value")
    field(DTYP, "stream")
    field(INP,  "@caenNdt1470.proto getAllSTAT($(BD), $(P)$(R)Ch0-Stat-R, $(P)$(R)Ch1-Stat-R, $(P)$(R)Ch2-Stat-R, $(P)$(R)Ch3-Stat-R) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
}

record(mbbiDirect, "$(P)$(R)Ch$(CH)-StatInit") {
    field(DESC, "Channel Status value")
    field(DTYP, "stream")
    field(PINI, "YES")
    field(INP,  "@caenNdt1470.proto getAllSTAT($(BD), $(P)$(R)Ch0-StatInit, $(P)$(R)Ch1-StatInit, $(P)$(R)Ch2-StatInit, $(P)$(R)Ch3-StatInit) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
}

record(bo, "$(P)$(R)Ch$(CH)-Enable-S") {
    field(DESC, "Set channel power On/Off")
    field(DTYP, "Soft Channel")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
    field(OUT,  "$(P)$(R)#Ch$(CH)-EnableWrite PP")
}

record(bo, "$(P)$(R)#Ch$(CH)-EnableWrite") {
    field(DESC, "Set channel power On/Off")
    field(DTYP, "stream")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
    field(OUT,  "@caenNdt1470.proto setSTAT($(BD), $(CH), $(P)$(R)Ch$(CH)-EnableMsg-R) $(PORT) $(ADDR)")
}

record(stringin, "$(P)$(R)Ch$(CH)-EnableMsg-R") {
    field(DESC, "On/Off Set status")
    field(DTYP, "Soft Channel")
}
