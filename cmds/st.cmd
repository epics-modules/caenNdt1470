require caenndt1470

#IP or hostname of the TCP endpoint.
epicsEnvSet(IPADDR,   "10.4.0.127")
#Port number for the TCP endpoint.
epicsEnvSet(IPPORT,   "1470")
#Prefix for EPICS PVs.
epicsEnvSet(P,          "$(P=Utg-Test:)")
epicsEnvSet(Dis,        "Ctrl")
epicsEnvSet(Dev,        "Caen1470PS")
epicsEnvSet(Bd,         "0")
epicsEnvSet(Idx,        "10"$(Bd))
epicsEnvSet(R,          "${Dis}-${Dev}-${Idx}:")
epicsEnvSet(PREFIX,     "$(P)$(R)")
epicsEnvSet(PORTNAME, "$(PREFIX)")
#Polling rate to monitor configuration and measurements.
epicsEnvSet(SCAN,     "1")
#for iocStats
epicsEnvSet(LOCATION, "UTG-ICS: $(IPADDR)")
epicsEnvSet(IOCNAME,  "$(PREFIX)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(caenndt1470_DIR)db")

# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

#Load your database defining the EPICS records
iocshLoad("$(caenndt1470_DIR)caenNdt1470.iocsh", "P=$(P), R=$(R), Bd=$(Bd), IPADDR=$(IPADDR), IPPORT=$(IPPORT), PORT=$(PORTNAME), SCAN=$(SCAN)")

iocInit()