# caenNdt1470

European Spallation Source ERIC Site-specific EPICS module: caenNdt1470

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/CAEN+NDT-1470+NIM+and+Desktop+HV+Power+Supply)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/caenNdt1470-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
